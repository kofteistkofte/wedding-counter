use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use web_sys::{Document, Element, Window};
use chrono::prelude::*;

#[wasm_bindgen(start)]
pub fn run() -> Result<(), JsValue> {
    let window = web_sys::window().expect("no global `window` exists!");
    let document = window.document().expect("should have a document on window");

    setup_clock(&window, &document)?;

    Ok(())
}

fn get_element(document: &Document, name: &str) -> Result<Element, JsValue> {
    Ok(document
        .get_element_by_id(name)
        .expect(&format!("There is no element \"{name}\""))
    )
}

fn update_time (wedding_time: &DateTime<Local>, day: &Element, hour: &Element, minute: &Element) {
    let current_time = Local::now();
    if wedding_time > &current_time {
        let diff = wedding_time.to_owned() - current_time;

        day.set_inner_html(&format!("{:02}", diff.num_days()));
        hour.set_inner_html(&format!("{:02}", diff.num_hours() % 24));
        minute.set_inner_html(&format!("{:02}", diff.num_minutes() % 60));
    }
}

fn setup_clock (window: &Window, document: &Document,) -> Result<(), JsValue> {
    let wedding_time = Local.ymd(2022, 8, 20).and_hms(18, 15, 00);

    let wedd_date = get_element(&document, "wedding-date")?;
    let day = get_element(&document, "day")?;
    let hour = get_element(&document, "hour")?;
    let minute = get_element(&document, "minute")?;

    wedd_date.set_inner_html(&format!(
        "{}", wedding_time.format_localized(
            "<b>%d %B %Y</b> %A Günü, Saat <b>%H:%M</b>",
            Locale::tr_TR
        ),
    ).trim());

    update_time(&wedding_time, &day, &hour, &minute);

    let interval = Closure::<dyn Fn()>::new(move || update_time(&wedding_time, &day, &hour, &minute));

    window.set_interval_with_callback_and_timeout_and_arguments_0(
        interval.as_ref().unchecked_ref(), 1000
    )?;

    interval.forget();
    Ok(())
}
